<?php

namespace ArmyProject\festa;


class festa extends Conexion{//se manden llamar metodos de una sola clase
		public $id;
        public $dia_act;
		public $nomb_act;
 		public $horario_festa;



function create(){
	    $pre = mysqli_prepare($this->con, "INSERT INTO festa(dia_actividad, nombre_actividad, horario_festa) VALUES (?,?,?)");
        $pre->bind_param("sss", $this->dia_act, $this->nomb_act, $this->horario_festa);//pasar parametros
        $pre->execute();
        $pre_ = mysqli_prepare($this->con, "SELECT LAST_INSERT_ID() ID_concierto");
        $pre_->execute();
        $r = $pre_ -> get_result();
        $this->id = $r -> fetch_assoc()["ID_festa"];
        return true;
    }

static function findDia($dia_act){
                $me = new Conexion();
                $pre = mysqli_prepare($me->con, "SELECT * FROM festa where dia_actividad=?");
                $pre->bind_param("s", $dia_act);
                $pre->execute();
                $re = $pre->get_result();
                return $re->fetch_object(festa::class);
    }
 function delete(){
            $pre = mysqli_prepare($this->con, "DELETE from festa WHERE id=?");
            $pre->bind_param("i", $this->id);
            $pre->execute();
            return true;
 }

 function update(){
            $pre = mysqli_prepare($this->con, "UPDATE festa SET dia_actividad = ?, nombre_actividad = ?, horario_festa = ? WHERE id = ?");
            $pre->bind_param("sss", $this->dia_act, $this->nombre_actividad, $this->horario_festa, $this->id);
            $pre->execute();
            return true;
 }

}

?>