<?php

namespace ArmyProject\concierto;


class concierto extends Conexion{//se manden llamar metodos de una sola clase
		public $id;
        public $nomb_tour;
		public $pais;
 		public $lugar;
        public $horario;
        public $fecha;


function create(){
	    $pre = mysqli_prepare($this->con, "INSERT INTO concierto(nombre_Tour, Pais, Lugar, Horario, Fecha) VALUES (?,?,?,?,?)");
        $pre->bind_param("sssss", $this->nomb_tour, $this->pais, $this->lugar, $this->horario, $this->fecha);//pasar parametros
        $pre->execute();
        $pre_ = mysqli_prepare($this->con, "SELECT LAST_INSERT_ID() ID_concierto");
        $pre_->execute();
        $r = $pre_ -> get_result();
        $this->id = $r -> fetch_assoc()["ID_concierto"];
        return true;
    }

static function findTour($nomb_tour){
                $me = new Conexion();
                $pre = mysqli_prepare($me->con, "SELECT * FROM concierto where nombre_Tour=?");
                $pre->bind_param("s", $nomb_tour);
                $pre->execute();
                $re = $pre->get_result();
                return $re->fetch_object(concierto::class);
    }
static function findPais($nomb_tour){
                $me = new Conexion();
                $pre = mysqli_prepare($me->con, "SELECT * FROM concierto where pais=?");
                $pre->bind_param("s", $pais);
                $pre->execute();
                $re = $pre->get_result();
                return $re->fetch_object(concierto::class);
    }
 function delete(){
            $pre = mysqli_prepare($this->con, "DELETE from concierto WHERE id=?");
            $pre->bind_param("i", $this->id);
            $pre->execute();
            return true;
 }

 function update(){
            $pre = mysqli_prepare($this->con, "UPDATE concierto SET nombre_Tour = ?, Pais = ?, Lugar = ?, Horario = ?, Fecha = ? WHERE id = ?");
            $pre->bind_param("sssss", $this->nomb_tour, $this->pais, $this->lugar, $this->horario, $this->fecha, $this->id);
            $pre->execute();
            return true;
 }

}

?>