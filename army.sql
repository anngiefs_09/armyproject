-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 13-02-2020 a las 00:31:38
-- Versión del servidor: 10.3.15-MariaDB
-- Versión de PHP: 7.3.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `army`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `army`
--

CREATE TABLE `army` (
  `id` int(3) NOT NULL,
  `user_tw` varchar(200) NOT NULL,
  `contrasenia` varchar(15) NOT NULL,
  `pais` varchar(60) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `army`
--

INSERT INTO `army` (`id`, `user_tw`, `contrasenia`, `pais`) VALUES
(6, 'anngiefs', 'minyoongi03', 'Corea Del Sur'),
(7, 'marjoonie99', 'marebangtan', 'Mexico City '),
(8, 'balabab', 'red09', 'Canada'),
(9, 'kim_namjoon', 'koalakoya', 'Corea Del Sur'),
(10, 'kim_seokjin', 'kimelguapo', 'Corea Del Sur'),
(11, 'min_yoongi', 'agustdym', 'Corea Del Sur'),
(12, 'jung_hoseok', 'hopeonthestreet', 'Corea Del Sur'),
(13, 'park_jimin', 'jimenoyyoon', 'Corea Del Sur'),
(14, 'kim_taehyung', 'purplearmyjk', 'Corea Del Sur'),
(15, 'jeon_jungkook', 'bananamilkth', 'Corea Del Sur');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `comeback`
--

CREATE TABLE `comeback` (
  `id_comeback` int(1) NOT NULL,
  `nombre_album` varchar(200) NOT NULL,
  `horario_album` datetime NOT NULL,
  `numero_canciones` int(3) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `consultaconciertos`
--

CREATE TABLE `consultaconciertos` (
  `ID_concierto` int(3) NOT NULL,
  `nombre_Tour` varchar(200) NOT NULL,
  `Pais` varchar(200) NOT NULL,
  `Lugar` varchar(200) NOT NULL,
  `Horario` datetime NOT NULL,
  `Fecha` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `festa`
--

CREATE TABLE `festa` (
  `ID_festa` int(3) NOT NULL,
  `dia_actividad` date NOT NULL,
  `nombre_actividad` varchar(60) NOT NULL,
  `horario_festa` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `presentaciones`
--

CREATE TABLE `presentaciones` (
  `ID_pre` int(1) NOT NULL,
  `tipo` varchar(200) NOT NULL,
  `pais_presentacion` varchar(200) NOT NULL,
  `horario_p` datetime NOT NULL,
  `fecha_p` date NOT NULL,
  `cantidad_nominacion` int(3) NOT NULL,
  `nominaciones` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `run`
--

CREATE TABLE `run` (
  `ID_run` int(3) NOT NULL,
  `numero_ep` int(5) NOT NULL,
  `horario_run` datetime NOT NULL,
  `dia_run` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `army`
--
ALTER TABLE `army`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `comeback`
--
ALTER TABLE `comeback`
  ADD PRIMARY KEY (`id_comeback`);

--
-- Indices de la tabla `consultaconciertos`
--
ALTER TABLE `consultaconciertos`
  ADD PRIMARY KEY (`ID_concierto`);

--
-- Indices de la tabla `festa`
--
ALTER TABLE `festa`
  ADD PRIMARY KEY (`ID_festa`);

--
-- Indices de la tabla `presentaciones`
--
ALTER TABLE `presentaciones`
  ADD PRIMARY KEY (`ID_pre`);

--
-- Indices de la tabla `run`
--
ALTER TABLE `run`
  ADD PRIMARY KEY (`ID_run`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `army`
--
ALTER TABLE `army`
  MODIFY `id` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT de la tabla `comeback`
--
ALTER TABLE `comeback`
  MODIFY `id_comeback` int(1) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `consultaconciertos`
--
ALTER TABLE `consultaconciertos`
  MODIFY `ID_concierto` int(3) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `festa`
--
ALTER TABLE `festa`
  MODIFY `ID_festa` int(3) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `presentaciones`
--
ALTER TABLE `presentaciones`
  MODIFY `ID_pre` int(1) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `run`
--
ALTER TABLE `run`
  MODIFY `ID_run` int(3) NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
