<?php

namespace ArmyProject\presentaciones;


class presentaciones extends Conexion{//se manden llamar metodos de una sola clase
		public $id;
        public $tipo;
		public $pais_pre;
 		public $horario_pre;
        public $fecha_pre;
        public $can_nom;
        public $nomina;



function create(){
	    $pre = mysqli_prepare($this->con, "INSERT INTO presentaciones(tipo, pais_presentacion, horario_p, fecha_p, cantidad_nominaciones, nominaciones) VALUES (?,?,?,?,?,?)");
        $pre->bind_param("ssssis", $this->tipo, $this->pais_pre, $this->horario_pre, $this->fecha_pre, $this->can_nom, $this->nomina);//pasar parametros
        $pre->execute();
        $pre_ = mysqli_prepare($this->con, "SELECT LAST_INSERT_ID() ID_pre");
        $pre_->execute();
        $r = $pre_ -> get_result();
        $this->id = $r -> fetch_assoc()["ID_pre"];
        return true;
    }

static function findTipo($tipo){
                $me = new Conexion();
                $pre = mysqli_prepare($me->con, "SELECT * FROM presentaciones where tipo=?");
                $pre->bind_param("s", $tipo);
                $pre->execute();
                $re = $pre->get_result();
                return $re->fetch_object(presentaciones::class);
    }
static function findNomina($nomina){
                $me = new Conexion();
                $pre = mysqli_prepare($me->con, "SELECT * FROM presentaciones where nominaciones=?");
                $pre->bind_param("s", $nomina);
                $pre->execute();
                $re = $pre->get_result();
                return $re->fetch_object(presentaciones::class);
    }
 function delete(){
            $pre = mysqli_prepare($this->con, "DELETE from presentaciones WHERE id=?");
            $pre->bind_param("i", $this->id);
            $pre->execute();
            return true;
 }

 function update(){
            $pre = mysqli_prepare($this->con, "UPDATE presentaciones SET tipo = ?, pais_presentacion = ?, horario_p = ?, fecha_p = ?, cantidad_nominaciones = ?, nominaciones = ? WHERE id = ?");
            $pre->bind_param("ssssss", $this->tipo, $this->pais_pre, $this->horario_pre, $this->fecha_pre, $this->can_nom, $this->nomina, $this->id);
            $pre->execute();
            return true;
 }

}

?>