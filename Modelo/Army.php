<?php

namespace Modelo;


class Army extends Conexion{//se manden llamar metodos de una sola clase
		public $id;
        public $user_tw;
		public $contrasenia;
 		



function create(){
	    $pre = mysqli_prepare($this->con, "INSERT INTO army(user_tw, contrasenia) VALUES (?,?)");
        $pre->bind_param("ss", $this->user_tw, $this->contrasenia);//pasar parametros
        $pre->execute();
        $pre_ = mysqli_prepare($this->con, "SELECT LAST_INSERT_ID() id");
        $pre_->execute();
        $r = $pre_ -> get_result();
        $this->id = $r -> fetch_assoc()["id"];
        return true;
    }

static function findUsuario($user_tw,$contrasenia){
                $me = new \Modelo\Conexion();
                $pre = mysqli_prepare($me->con, "SELECT * FROM army WHERE user_tw=? AND contrasenia=? ");
                $pre->bind_param("ss", $user_tw, $contrasenia);
                $pre->execute();
                $res = $pre->get_result();
                if(mysqli_num_rows($res)>0){
                return $res->fetch_object(Army::class);
                 }
 }   
 static function findID($id){
                $me = new \Modelo\Conexion();
                $pre = mysqli_prepare($me->con, "SELECT *FROM army WHERE id=?");
                $pre ->bind_param("i", $id);
                $pre->execute();
                $res = $pre->get_result();
                return $res->fetch_object(Army::class);
    }

 static function findtodo(){
                $me = new \Modelo\Conexion();
                $query =  "SELECT * FROM army";
                $resul=mysqli_query($me->con, $query);
            if(!$resul){
                die('Query Error'.mysqli_error($me->con));
            }
            $json = array();
            while($row = mysqli_fetch_array($resul)){
                $json[] = array(
                        'user_tw'=>$row['user_tw'],
                        'contrasenia'=>$row['contrasenia'],
                        'id'=> $row['id'],
                   );     
                 }
                 echo json_encode($json);  
            }

 static function delete($id){
            $me=new\Modelo\Conexion();
            $pre = mysqli_prepare($me->con, "DELETE FROM army WHERE id =?");
            $pre->bind_param("i", $id);
            $pre->execute();
            
 }

 function update(){
            $pre = mysqli_prepare($this->con, "UPDATE army SET user_tw = ?, contrasenia = ? WHERE id = ?");
            $pre->bind_param("ssi", $this->user_tw, $this->contrasenia, $this->id);
            $pre->execute();
            return true;
 }


 static function findNombre($buscar)
        {
            if(!empty($buscar)){
            $me= new \Modelo\Conexion();
            $query = "SELECT * FROM army WHERE user_tw LIKE '$buscar%'";
            $resul=mysqli_query($me->con, $query);
                if(!$resul){
                    die('Query Error'.mysqli_error($me->con));
                }
                $json = array();
                while($row = mysqli_fetch_array($resul)){
                    $ob = array(
                        'id'=> $row['id'],
                        'user_tw'=>$row['user_tw'],
                        'contrasenia'=>$row['contrasenia'],
                        
                    );
                    array_push($json, $ob); 
                }
                echo json_encode($json);
                
            }
        }
}


?>