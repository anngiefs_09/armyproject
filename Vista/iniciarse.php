<!doctype html>
<html lang="en">
<head>
    <title>INICIAR</title>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no" />
    <link rel="stylesheet" href="assets/css/main.css" />
</head>
<body class="landing is-preload">
<div id="page-wrapper">

    <!-- Header -->
    <header id="header" class="alt xlarge">
            <nav id="nav">
                <ul>
                    <li><a href="iniciarse.html" class="button">INICIAR SESIÓN</a></li>
                    <li><a href="contact.html" class="button">REGISTRATE</a></li>
                </ul>
            </nav>
                </header>

    <br/><br/><br/><br/><br/><br/>
    <!-- Main -->
    
    <section id="main" class="container medium">
            <br/><br/>
           <center><h3>INICIAR SESIÓN</h3></center>
        <div class="box">

            <form id="iniciar"  action="http://localhost/ArmyProject/index.php?controller=Army&action=insertararmy" method="Get">
                <div class="row gtr-50 gtr-uniform">
                    <?php if (isset($_SESSION["flash"])){ ?>
                        <?php
                        echo $_SESSION["flash"];
                        unset($_SESSION["flash"]);//Manda el mensaje Flash y la sesion se inicia
                        ?>
                    <?php } ?>

                        <center><input type="text" name="user_tw" id="user_tw" value="" placeholder="Usuario"/><br/></center>
                        <center><input type="password" name="contrasenia" id="contrasenia" value="" placeholder="Contraseña"/><br/></center>
                        <ul class="actions special">
                            <a class="button " type="submit" id="entrar">ENTRAR</a>
                        </ul>
                    </div>
                </form>
                </div>
            </section>
        </div>
    </section>


    <!-- Footer -->
    <footer id="footer">
                <ul class="icons">
                    <li><a href="https://twitter.com/BTS_twt" class="icon brands fa-twitter">Twitter<span class="label"></span></a></li>
                    <li><a href="https://www.facebook.com/bangtan.official/" class="icon brands fa-facebook-f">Facebook<span class="label">Facebook</span></a></li>
                    <li><a href="https://www.instagram.com/bts.bighitofficial/?hl=es-la" class="icon brands fa-instagram">Instagram<span class="label">Instagram</span></a></li>
                </ul>
                <ul class="copyright">
                    <li>&copy;ARMY.</li>
                </ul>
            </footer>

</div>

<!-- Scripts -->
<script src="assets/js/jquery.min.js"></script>
<script src="assets/js/jquery.dropotron.min.js"></script>
<script src="assets/js/jquery.scrollex.min.js"></script>
<script src="assets/js/browser.min.js"></script>
<script src="assets/js/breakpoints.min.js"></script>
<script src="assets/js/util.js"></script>
<script src="assets/js/main.js"></script>

</body>
<script type="application/javascript">
        $(document).ready(function(){ //Se hace referencia a jQuery que es un framework
            $("#entrar [type='submit']").on("click",function(e){
            e.preventDefault(); //Se hace referencia por id que se llama entrar
                var user_tw=$("#entrar input[name='user_tw']"); //Se declaran las variables
                var contrasenia=$("#entrar input[name='contrasenia']");
                    if (user_tw.val() == "" || contrasenia.val() == "") {
                        alert("El formulario aun no esta completo");
                    }
                    
                    $("#entrar").submit();
            });
        });
    </script>
</html>