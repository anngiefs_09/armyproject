<?php

namespace ArmyProject\Modelo;


class comeback extends Conexion{//se manden llamar metodos de una sola clase
		public $id;
        public $nomb_alb;
		public $alb_dt;
 		public $num_can;



function create(){
	    $pre = mysqli_prepare($this->con, "INSERT INTO comeback(nombre_album, album_dt, numero_canciones) VALUES (?,?,?)");
        $pre->bind_param("ssi", $this->nombre_album, $this->album_dt, $this->numero_canciones);//pasar parametros
        $pre->execute();
        $pre_ = mysqli_prepare($this->con, "SELECT LAST_INSERT_ID() id_comeback");
        $pre_->execute();
        $r = $pre_ -> get_result();
        $this->id = $r -> fetch_assoc()["id_comeback"];
        return true;
    }

static function findAlbum($nomb_alb){
                $me = new Conexion();
                $pre = mysqli_prepare($me->con, "SELECT * FROM comeback where nombre_album=?");
                $pre->bind_param("s", $nomb_alb);
                $pre->execute();
                $re = $pre->get_result();
                return $re->fetch_object(comeback::class);
    }
 function delete(){
            $pre = mysqli_prepare($this->con, "DELETE from comeback WHERE id=?");
            $pre->bind_param("i", $this->id);
            $pre->execute();
            return true;
 }

 function update(){
            $pre = mysqli_prepare($this->con, "UPDATE comeback SET nombre_album = ?, album_dt = ?, numero_canciones = ? WHERE id = ?");
            $pre->bind_param("ssii", $this->nomb_alb, $this->alb_dt, $this->num_can, $this->id);
            $pre->execute();
            return true;
 }

}

?>